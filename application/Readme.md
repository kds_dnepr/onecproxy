1. composer install
2.
`mkdir -p config/jwt` 

`openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096`

`openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout`
3. config .env (DATABASE_URL, BASE_PATH_ONE_C, JWT_PASSPHRASE)
4. php bin/console make:migration
   php bin/console doctrine:migrations:migrate
5.