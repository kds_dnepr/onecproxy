<?php

namespace App\Helpers;

use http\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedBase64File extends UploadedFile
{
    protected $filePath;

    /**
     * UploadedBase64File constructor.
     * @param string $base64String
     * @param string $originalName
     * @param string $extension
     * @param string $group
     */
    public function __construct(string $base64String, string $originalName, string $extension, string $group)
    {
        $dir = $_ENV['WEB_ROOT_PATH'] . 'content/' . $group;
        $this->checkOrCreateDir($dir);

        $this->filePath = $dir . '/' . $originalName . '.' . $extension;

        if(file_exists($this->filePath)){
            throw new \RuntimeException('Файл с таким именем уже существует.');
        }
        $data = base64_decode($base64String);
        file_put_contents($this->filePath, $data);

        $error = null;
        $mimeType = null;
        $test = true;

        parent::__construct($this->filePath, $originalName, $mimeType, $error, $test);
    }

    /**
     * @param string $path
     * @return int
     */
    private function checkOrCreateDir(string $path): int
    {
        if (!file_exists($path)) {
            if (!mkdir($path, 0755) && !is_dir($path)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
            }
        }
        return 1;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

}