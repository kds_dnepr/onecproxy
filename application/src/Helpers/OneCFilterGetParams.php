<?php

namespace App\Helpers;

class OneCFilterGetParams
{

    protected $exclude = ['Фото'];

    public function filter(array &$getParams)
    {
        if (isset($getParams['$expand'])) {
            $expand = explode(',', $getParams['$expand']);
            $getParams['$expand'] = implode(',', array_diff($expand, $this->exclude));
            return true;
        }
        return false;
    }

}