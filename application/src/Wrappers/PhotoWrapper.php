<?php

namespace App\Wrappers;

use App\Entity\File;
use Doctrine\ORM\EntityManagerInterface;
use yii\helpers\ArrayHelper;

class PhotoWrapper
{
    private $refKey = 'Фото_Key';

    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function wrap(array $proxyResponse): array
    {
        if (isset($proxyResponse['value']) && count($proxyResponse['value']) > 0) {
            $fotoKeys = array_column($proxyResponse['value'], $this->refKey);
            $photos = $this->em
                ->getRepository(File::class)
                ->findByFileRefKey($fotoKeys)
                ->getQuery()
                ->getArrayResult();

            $photoIndexByRefKey = ArrayHelper::index($photos, 'fileRefKey');
            foreach ($proxyResponse['value'] as &$item) {
                if (isset($photoIndexByRefKey[$item['Фото_Key']])) {
                    $webPath = '/' . str_replace(
                            $_ENV['WEB_ROOT_PATH'],
                            '',
                            $photoIndexByRefKey[$item['Фото_Key']]['path']
                        );

                    $item['Фото'] = $webPath;
                }
            }
        }
        return $proxyResponse;
    }
}