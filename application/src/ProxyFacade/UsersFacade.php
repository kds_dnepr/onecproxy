<?php

namespace App\ProxyFacade;


use App\Clients\OneCClient;
use App\VO\User\OneCCredential;
use yii\helpers\ArrayHelper;

class UsersFacade
{
    const ONE_C_PATH = 'Catalog_Пользователи';

    /**
     * @var OneCClient
     */
    private $apiClient;

    /**
     * UserFacade constructor.
     * @param $apiClient
     */
    public function __construct(OneCClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param string|null $roleName
     * @return string|null
     */
    public function rolesMap(string $roleName = null): ?string
    {
        $mapList = array(
            'АДМИНИСТРАЦИЯ' => 'ADMINISTRATION_ROLE',
            'РУКОВОДИТЕЛИ' => 'RUKOVODITEL_ROLE',
            'Интернет-Пользователи' => 'NET_USER_ROLE',
            'ПОЛЬЗОВАТЕЛИ ПРОГРАММЫ' => 'USER_ROLE',
            'Boss24' => 'BOSS24'
        );

        if (isset($roleName, $mapList[$roleName])) {
            return $mapList[$roleName];
        }

        return null;
    }

    /**
     * @param OneCCredential $userCredential
     * @return string|null
     */
    public function getRolesByUser(OneCCredential $userCredential): ?string
    {
        $response = $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH
        );

        if (isset($response['value']) && count($response['value']) > 0) {
            $users = $response['value'];

            $userIndexByCode = ArrayHelper::index($users, 'Code');
            $userIndexByKey = ArrayHelper::index($users, 'Ref_Key');
            $userName = strtoupper($userCredential->getUsername());
            $userParentKey = $userIndexByCode[$userName]['Parent_Key'];

            if ($userParentKey !== '00000000-0000-0000-0000-000000000000') {
                $roleName = $userIndexByKey[$userParentKey]['Description'];
                return $this->rolesMap($roleName);
            }
            return null;
        }
    }

    /**
     * @param OneCCredential $userCredential
     * @return string|null
     */
    public function getRolesTree(OneCCredential $userCredential)
    {
        $response = $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH
        );

        if (isset($response['value']) && count($response['value']) > 0) {
            $users = $response['value'];

            $userIndexByKey = ArrayHelper::index($users, 'Ref_Key');

            $userIndexByDescription = ArrayHelper::index($users, 'Description');
            array_walk($userIndexByDescription, function (&$item) use ($userIndexByKey) {
                if ($item['Parent_Key'] !== '00000000-0000-0000-0000-000000000000') {
                    $item['Parent_Key'] = $userIndexByKey[$item['Parent_Key']]['Description'];
                }
            });

            $tree = array_flip(
                array_keys(
                    array_filter($userIndexByDescription, function ($item) {
                        return $item['Parent_Key'] === '00000000-0000-0000-0000-000000000000';
                    })
                ));

            return $this->buildTree($tree, $userIndexByDescription, $tree);
        }
    }

    /**
     * @param $parent
     * @param $all
     * @param $tree
     * @return mixed
     */
    private function buildTree($parent, array $all, &$tree)
    {
        foreach ($parent as $key => $item) {
            $child = array_flip($this->getChildById($key, $all));
            array_walk_recursive($tree, function (&$v, $k) use ($key, $child) {
                if (($k === $key) && (count($child) > 0)) {
                    $v = $child;
                }
            });
            if (count($child) > 0) {
                $this->buildTree($child, $all, $tree);
            }
        }
        return $tree;
    }

    /**
     * @param $id
     * @param $allUser
     * @return array
     */
    private function getChildById($id, $allUser): array
    {
        return array_column(
            array_filter($allUser, function ($item) use ($id) {
                return $item['Parent_Key'] === $id;
            }), 'Description'
        );
    }

}