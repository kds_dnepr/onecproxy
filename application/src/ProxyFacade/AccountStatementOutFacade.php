<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\Entity\CompanyAccount;
use App\Entity\CompanyAccountStatements;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class AccountStatementOutFacade
{
    const ONE_C_PATH = 'Document_ПлатежноеПоручениеИсходящее';

    /**
     * @var OneCClient
     */
    private $apiClient;
    /**
     * @var User
     */
    private $adminUser;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * UserFacade constructor.
     * @param OneCClient $apiClient
     * @param EntityManagerInterface $em
     */
    public function __construct(OneCClient $apiClient, EntityManagerInterface $em)
    {
        $this->apiClient = $apiClient;
        $this->em = $em;
        $this->adminUser = $this->em
            ->getRepository(User::class)
            ->getAdmin();
    }

    public function createAccountStatementOut(CompanyAccountStatements $accountStatements,string $bAccRefKey, $bContragentRefKey)
    {
        $postData = [
            "Number"=> $accountStatements->getNumOrder(),
            "Date"=> $accountStatements->getDateOrder(),
            "Posted"=> true,
            "Сумма"=> $accountStatements->getSum(),
            "Организация_Key"=> "00000000-0000-0000-0000-000000000000",
            "Автор_Key"=> "306bf4d1-40d5-11ea-af6d-00155dd6c512",
            "Контрагент_Key"=> $bContragentRefKey,
            "Счет_Key"=> $bAccRefKey,
            "Основание"=> "",
            "Основание_Type"=> "StandardODATA.Undefined",
            "ОтправительСредств_Key"=> $accountStatements->getAAccount()->getCompany()->getRefKey(),
            "СтатьяДДС_Key"=> "00000000-0000-0000-0000-000000000000",
            "Комментарий"=> $accountStatements->getDescription(),
            "АвторТекст"=> "ГРОМОВ ОЛЕГ",
            "Взаиморасчеты"=> [],
        ];

        return $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            [],
            $postData
        );
    }


}