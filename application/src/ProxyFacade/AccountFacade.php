<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\Entity\CompanyAccount;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class AccountFacade
{
    const ONE_C_PATH = 'Catalog_Счета';

    /**
     * @var OneCClient
     */
    private $apiClient;
    /**
     * @var User
     */
    private $adminUser;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * UserFacade constructor.
     * @param OneCClient $apiClient
     * @param EntityManagerInterface $em
     */
    public function __construct(OneCClient $apiClient, EntityManagerInterface $em)
    {
        $this->apiClient = $apiClient;
        $this->em = $em;
        $this->adminUser = $this->em
            ->getRepository(User::class)
            ->getAdmin();
    }

    public function createAccount(CompanyAccount $account)
    {
        $postData = [
            "Parent_Key" => '00000000-0000-0000-0000-000000000000',
            "IsFolder" => false,
            "Description" => $account->getName(),
            "БанкСчет" => $account->getAccountNumber(),
            "БезНал" => true,
            "БанкМФО" => $account->getMfo(),
            "БанкНазвание" => $account->getBankName(),
            "БанкАдрес" => "",
            "Банк_Key" => BankFacade::BANK_KEY[$account->getAlias()],
            "Организация_Key" => "00000000-0000-0000-0000-000000000000",
            "КассаВПути" => false,
        ];

        return $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            [],
            $postData
        );
    }

    public function createAccountRaw($description, $accNum, $mfo, $bankName, $bankKey)
    {
        $postData = [
            "Parent_Key" => '00000000-0000-0000-0000-000000000000',
            "IsFolder" => false,
            "Description" => $description,
            "БанкСчет" => $accNum,
            "БезНал" => true,
            "БанкМФО" => $mfo,
            "БанкНазвание" => $bankName,
            "БанкАдрес" => "",
            "Банк_Key" => $bankKey,
            "Организация_Key" => "00000000-0000-0000-0000-000000000000",
            "КассаВПути" => false,
        ];

        return $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            [],
            $postData
        );
    }

    public function getAllAccount()
    {
        $response = $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            ['$filter' => 'IsFolder eq false']
        );
        return $response['value'];
    }

}