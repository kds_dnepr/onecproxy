<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\VO\User\OneCCredential;
use Symfony\Component\HttpFoundation\Request;

class UploadedDataFacade
{
    const ONE_C_PATH = 'Catalog_ПрикрепленныеДанные';

    /**
     * @var OneCClient
     */
    private $apiClient;

    /**
     * UserFacade constructor.
     * @param $apiClient
     */
    public function __construct(OneCClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param OneCCredential $userCredential
     * @param array $postData
     * @return array|null
     */
    public function addNewFile(OneCCredential $userCredential, array $postData): ?array
    {
        return $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH,
            Request::METHOD_POST,
            [],
            $postData
        );
    }

    /**
     * @param OneCCredential $userCredential
     * @param int $skip
     * @param int $count
     * @return array|null
     */
    public function getFiles(OneCCredential $userCredential, int $skip, int $count): ?array
    {
        return $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH,
            Request::METHOD_GET,
            [
                '$skip' => $skip,
                '$top' => $count
            ]
        );
    }

}