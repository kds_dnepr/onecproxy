<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\VO\User\OneCCredential;
use http\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use yii\helpers\ArrayHelper;

class EmployeeFacade
{
    const ONE_C_PATH = 'Catalog_Сотрудники';

    /**
     * @var OneCClient
     */
    private $apiClient;

    /**
     * @var UsersFacade
     */
    private $userFacade;

    /**
     * UserFacade constructor.
     * @param $apiClient
     */
    public function __construct(OneCClient $apiClient, UsersFacade $usersFacade)
    {
        $this->apiClient = $apiClient;
        $this->userFacade = $usersFacade;
    }

    /**
     * @param OneCCredential $userCredential
     * @return string|null
     */
    public function getRolesTree(OneCCredential $userCredential): ?array
    {
        $response = $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH
        );

        if (isset($response['value']) && count($response['value']) > 0) {
            $users = $response['value'];

            $users = array_filter($users, function ($value){
                return $value['IsFolder'] === true;
            });

            $userIndexByKey = ArrayHelper::index($users, 'Ref_Key');

            $userIndexByDescription = ArrayHelper::index($users, 'Description');
            array_walk($userIndexByDescription, function (&$item) use ($userIndexByKey) {
                if ($item['Parent_Key'] !== '00000000-0000-0000-0000-000000000000') {
                    $item['Parent_Key'] = $userIndexByKey[$item['Parent_Key']]['Description'];
                }
            });

            $tree = array_flip(
                array_keys(
                    array_filter($userIndexByDescription, function ($item) {
                        return $item['Parent_Key'] === '00000000-0000-0000-0000-000000000000';
                    })
                ));

            return $this->buildTree($tree, $userIndexByDescription, $tree);
        }
    }

    /**
     * @param $parent
     * @param $all
     * @param $tree
     * @return mixed
     */
    private function buildTree($parent, array $all, &$tree)
    {
        foreach ($parent as $key => $item) {
            $child = array_flip($this->getChildById($key, $all));
            array_walk_recursive($tree, function (&$v, $k) use ($key, $child) {
                if (($k === $key) && (count($child) > 0)) {
                    $v = $child;
                }
            });
            if (count($child) > 0) {
                $this->buildTree($child, $all, $tree);
            }
        }
        return $tree;
    }

    /**
     * @param string $id
     * @param array $allUser
     * @return array
     */
    private function getChildById(string $id, array $allUser): array
    {
        return array_column(
            array_filter($allUser, function ($item) use ($id) {
                return $item['Parent_Key'] === $id;
            }), 'Description'
        );
    }

    public function getRolesByCode(OneCCredential $userCredential, string $employeeKey)
    {
        $response = $this->apiClient->send(
            $userCredential,
            self::ONE_C_PATH
        );
        if (isset($response['value']) && count($response['value']) > 0) {
            $employees = $response['value'];

            $employeeIndexByKey = ArrayHelper::index($employees, 'Ref_Key');
            if (isset($employeeIndexByKey[$employeeKey])) {
                $employeeParent = $employeeIndexByKey[$employeeKey]['Parent_Key'];
                if (isset($employeeIndexByKey[$employeeParent])) {
                    return $employeeIndexByKey[$employeeParent]['Description'];
                }else {
                    throw new \RuntimeException('Roles not found');
                }
            } else {
                throw new \RuntimeException('User not found');
            }
        }

        return null;

    }

}