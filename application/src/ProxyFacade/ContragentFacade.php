<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\Entity\Company;
use App\Entity\CompanyAccount;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class ContragentFacade
{
    const ONE_C_PATH = 'Catalog_Контрагенты';

    /**
     * @var OneCClient
     */
    private $apiClient;
    /**
     * @var User
     */
    private $adminUser;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * UserFacade constructor.
     * @param OneCClient $apiClient
     * @param EntityManagerInterface $em
     */
    public function __construct(OneCClient $apiClient, EntityManagerInterface $em)
    {
        $this->apiClient = $apiClient;
        $this->em = $em;
        $this->adminUser = $this->em
            ->getRepository(User::class)
            ->getAdmin();
    }

    public function createContragent(Company $company)
    {
        $postData = [
            "Parent_Key"=> "00000000-0000-0000-0000-000000000000",
            "IsFolder"=> false,
            "Description"=> $company->getName(),
            "Адрес"=> "",
            "ИНН"=> $company->getOkpo(),
            "ОКПО"=> $company->getOkpo(),
            "КодПанАгент"=> "",
            "ID"=> 0,
            "email"=> $company->getEmail(),
            "ФизАдрес"=> "",
            "Телефоны"=> $company->getPhone(),
            "ЭтоНашаФирма"=> false,
            "СуммаКредита"=> 0,
            "ДисконтнаяКарта"=> "0",
            "СуммаКредитаНам"=> 0,
            "НаименованиеЦерезит"=> "",
            "Менеджер_Key"=> "00000000-0000-0000-0000-000000000000",
            "ПолнНаименование"=>  $company->getName(),
            "НомерСвидетельства"=> "",
            "СтавкаНДС"=> 0,
            "ГоловнойКонтрагент_Key"=> "00000000-0000-0000-0000-000000000000",
            "КонтактноеЛицо"=> "",
            "Директор"=> "",
            "Директор_Type"=> "StandardODATA.Undefined",
            "Бухгалтер"=> "",
            "Бухгалтер_Type"=> "StandardODATA.Undefined",
            "ОбразецПечати_Key"=> "00000000-0000-0000-0000-000000000000",
            "МестоСоставленияДокументов"=> "",
            "КодЦерезит"=> "",
            "Организация_Key"=> "00000000-0000-0000-0000-000000000000",
            "СтатьяДДС_Key"=> "00000000-0000-0000-0000-000000000000",
            "МенеджерХенкель_Key"=> "00000000-0000-0000-0000-000000000000",
            "МенеджерОбщийПрайс_Key"=> "00000000-0000-0000-0000-000000000000",
            "ОтветственныйЗаКредит_Key"=> "00000000-0000-0000-0000-000000000000",
            "РасчетныеСчета"=> [],
            "ТелефоныСписок"=> [],
        ];

        return $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            [],
            $postData
        );
    }

    public function getAllContragent()
    {
        $response = $this->apiClient->send(
            $this->adminUser->getOneCCredential(),
            self::ONE_C_PATH,
            Request::METHOD_POST,
            ['$filter' => 'IsFolder eq false']
        );
        return $response['value'];
    }

}