<?php

namespace App\ProxyFacade;

use App\Clients\OneCClient;
use App\Entity\CompanyAccount;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class BankFacade
{
    const ONE_C_PATH = 'Catalog_Банки';

    /**
     * @var OneCClient
     */
    private $apiClient;

    const BANK_KEY = [
        'privat' => '747db543-6094-11ea-b6ba-00155dd6c512',
        'АТ КБ ПРИВАТБАНК' => '747db543-6094-11ea-b6ba-00155dd6c512',
        'aval' => 'c4d7d9af-65ac-11ea-b6ba-00155dd6c512',
        'ukrsibbank' => 'd6d5cc9c-65ac-11ea-b6ba-00155dd6c512'
    ];

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * UserFacade constructor.
     * @param OneCClient $apiClient
     * @param EntityManagerInterface $em
     */
    public function __construct(OneCClient $apiClient, EntityManagerInterface $em)
    {
        $this->apiClient = $apiClient;
        $this->em = $em;

    }

}