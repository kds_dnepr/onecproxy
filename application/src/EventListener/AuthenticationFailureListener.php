<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;

class AuthenticationFailureListener
{

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
//        $data = [
//            'message' => 'Неправильные авторизационные данные.',
//        ];

        $response = new JWTAuthenticationFailureResponse('Неправильный логин пользователя или пароль.');

        $event->setResponse($response);
    }

}