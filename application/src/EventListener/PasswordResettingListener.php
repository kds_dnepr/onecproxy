<?php

namespace App\EventListener;

use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class PasswordResettingListener implements EventSubscriberInterface
{

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::RESETTING_SEND_EMAIL_COMPLETED => 'onPasswordResettingSuccess',
        );
    }

    public function onPasswordResettingSuccess($event)
    {
        $event->setResponse(new JsonResponse(array('message' => 'reset done'), 200));
    }
}