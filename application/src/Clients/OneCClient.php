<?php

namespace App\Clients;

use App\VO\User\OneCCredential;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\Request;

class OneCClient
{
    private $basePath;

    public function __construct()
    {
        $this->basePath = $_ENV['BASE_PATH_ONE_C'];
    }

    private function defaultGetParams()
    {
        return ['$format' => 'json'];
    }

    /**
     * @param OneCCredential $credential
     * @param string $path
     * @param string $method
     * @param array $getParams
     * @param array $postParams
     * @param string $refKey
     * @return array
     */
    public function send(OneCCredential $credential,
                         string $path,
                         string $method = Request::METHOD_GET,
                         array $getParams = [],
                         array $postParams = [],
                         string $refKey = ''
    ): ?array
    {
        try {
            $client = new Client();

            $requestPath = $this->basePath . '/' . $path;
            if (!empty($refKey) && ($method === Request::METHOD_PUT || $method === Request::METHOD_PATCH)) {
                $requestPath .= '(guid:\'' . $refKey . '\')';
            }
            $response = $client->request($method, $requestPath, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'auth' => [
                    $credential->getUsername(),
                    $credential->getPassword()
                ],
                'json' => $postParams,
                'query' => array_merge($this->defaultGetParams(), $getParams),
            ]);
            if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
                return json_decode($response->getBody()->getContents(), true);
            }
        } catch (ConnectException $e) {
            throw new \RuntimeException('Сервер 1С не отвечает, попробуйте позже');
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === 401) {
                throw new \RuntimeException('Неверные авторизационные данные 1С');
            } elseif ($e->getResponse()->getStatusCode() === 400) {
                throw new \RuntimeException('Неверные данные в теле запроса');
            }
        } catch (ServerException $e) {
            throw new \RuntimeException($e->getResponse()->getBody());
        } catch (BadResponseException $e) {
            throw new \RuntimeException($e->getResponse()->getBody());
        }

    }
}