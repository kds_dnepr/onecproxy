<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\Request;

class CBBoss24Client
{
    private $basePath;
    private $apiKey;

    public function __construct()
    {
        $this->basePath = $_ENV['BASE_PATH_CB_BOSS24'];
        $this->apiKey = $_ENV['CB_BOSS24_API_KEY'];
    }

    /**
     * @param string $path
     * @param string $method
     * @param array $getParams
     * @return array
     */
    private function send(string $path,
                          string $method = Request::METHOD_GET,
                          array $getParams = []
    ): ?array
    {
        try {
            $client = new Client();

            $requestPath = $this->basePath . '/' . $path;
            $response = $client->request($method, $requestPath, [
                'headers' => [
                    'Accept' => 'application/json',
                    'apiKey' => $this->apiKey
                ],
//                'json' => $postParams,
                'query' => $getParams,
            ]);
            if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
                return json_decode($response->getBody()->getContents(), true);
            }
        } catch (ConnectException $e) {
            throw new \RuntimeException('Сервер ClientBankBoss24 не отвечает, попробуйте позже');
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === 404) {
                throw new \RuntimeException('Неверный apiKey');
            }
            if ($e->getResponse()->getStatusCode() === 400) {
                throw new \RuntimeException('Неверные данные в теле запроса');
            }
        } catch (ServerException $e) {
            throw new \RuntimeException($e->getResponse()->getBody());
        } catch (BadResponseException $e) {
            throw new \RuntimeException($e->getResponse()->getBody());
        }

    }

    public function getCompanyes()
    {
        return $this->send('/company/');
    }

    public function getCompany($companyOKPO)
    {
        return $this->send('/company/' . $companyOKPO);
    }

    public function getCompanyAccount(int $companyOKPO)
    {
        return $this->send('/company/' . $companyOKPO . '/account/');
    }

    public function getCompanyAccountStatements(int $companyOKPO, string $account, $dateFrom = null, $dateTo = null)
    {
        $getParams = [
            'account_number' => $account,
        ];

        if (isset($dateFrom) && isset($dateFrom)) {
            $getParams['filter[start_date]'] = $dateFrom;
            $getParams['filter[end_date]'] = $dateTo;
        }

        return $this->send(
            'company/' . $companyOKPO . '/statements',
            Request::METHOD_GET,
            $getParams
            );
    }
}