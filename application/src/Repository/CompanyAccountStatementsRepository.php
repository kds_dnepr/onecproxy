<?php

namespace App\Repository;

use App\Entity\CompanyAccountStatements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyAccountStatements|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyAccountStatements|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyAccountStatements[]    findAll()
 * @method CompanyAccountStatements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyAccountStatementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyAccountStatements::class);
    }

    // /**
    //  * @return CompanyAccountStatements[] Returns an array of CompanyAccountStatements objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyAccountStatements
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
