<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\UserBundle\Model\UserManagerInterface;


class UserRepository extends ServiceEntityRepository
{
    private $userManager;

    private $adminLogin;

    public function __construct(ManagerRegistry $registry, UserManagerInterface $userManager)
    {
        parent::__construct($registry, User::class);
        $this->userManager = $userManager;
        $this->adminLogin = $_ENV['LOGIN_ADMIN_ONE_C_CREDENTIAL'];
    }

    public function create(User $user)
    {
        $this->userManager->updateUser($user);
    }

    public function getAdmin()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username=:username')
            ->setParameter('username', $this->adminLogin)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
