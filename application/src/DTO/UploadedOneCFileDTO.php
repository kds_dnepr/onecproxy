<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UploadedOneCFileDTO
 * @package App\DTO
 */
class UploadedOneCFileDTO
{
    /**
     * @var string
     */
    protected $base64File;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $extension;


    /**
     * @var string
     */
    protected $entityName;

    /**
     * @var string
     */
    protected $entityRefKey;

    /**
     * @var string
     */
    protected $fileRefKey;

    /**
     * @var string
     */
    protected $code;

    /**
     * UploadBase64FileDTO constructor.
     * @param string $base64File
     * @param string $name
     * @param string $extension
     * @param string $entityName
     * @param string $entityRefKey
     * @param string $fileRefKey
     * @param string $code
     */
    public function __construct(
        string $base64File,
        string $name,
        string $extension,
        string $entityName,
        string $entityRefKey,
        string $fileRefKey,
        string $code)
    {
        $this->base64File = $base64File;
        $this->name = $name;
        $this->extension = $extension;
        $this->entityName = $entityName;
        $this->entityRefKey = $entityRefKey;
        $this->fileRefKey = $fileRefKey;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getBase64File(): string
    {
        return $this->base64File;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getFileRefKey(): string
    {
        return $this->fileRefKey;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return str_replace('StandardODATA.','',$this->entityName);
    }

    /**
     * @return string
     */
    public function getEntityRefKey(): string
    {
        return $this->entityRefKey;
    }
}