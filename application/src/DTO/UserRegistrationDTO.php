<?php
namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use App\VO\User\OneCCredential;

class UserRegistrationDTO
{
    /**
     * @Assert\Email(
     *     message = "Неправильный формат почты '{{ value }}' ",
     *     checkMX = true
     * )
     * @var string
     */
    protected $username;

    /**
     * @Assert\Length(
     *     min=4,
     *     max=20,
     *     minMessage="Длина пароля должна быть больше 4 символов",
     *     maxMessage = "Длина пароля должна быть меньше 20 символов"
     * )
     * @var string
     */
    protected $password;

    /**
     * @Assert\Length(
     *     min=4,
     *     max=20,
     *     minMessage="Длина логина 1С должна быть больше 4 символов",
     *     maxMessage = "Длина логина 1С должна быть меньше 20 символов"
     * )
     * @var string
     */
    protected $oneCUsername;

    /**
     * @Assert\Length(
     *     min=4,
     *     max=20,
     *     minMessage="Длина пароля 1С должна быть больше 4 символов",
     *     maxMessage = "Длина пароля 1С должна быть меньше 20 символов"
     * )
     * @var string
     */
    protected $oneCPassword;


    /**
     * RegistrationUserDTO constructor.
     * @param string $username
     * @param string $password
     * @param string $oneCUsername
     * @param string $oneCPassword
     */
    public function __construct(string $username, string $password, string $oneCUsername, string $oneCPassword)
    {
        $this->username = $username;
        $this->password = $password;
        $this->oneCUsername = $oneCUsername;
        $this->oneCPassword = $oneCPassword;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getOneCUsername(): string
    {
        return $this->oneCUsername;
    }

    /**
     * @return string
     */
    public function getOneCPassword(): string
    {
        return $this->oneCPassword;
    }


}
