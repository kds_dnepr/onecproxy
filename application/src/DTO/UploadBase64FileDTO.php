<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UploadBase64FileDTO
 * @package App\DTO
 */
class UploadBase64FileDTO
{
    /**
     * @Assert\NotBlank(
     *     message="Содержимое файла base64 должно быть заполнено"
     * )
     * @var string
     */
    protected $base64File;

    /**
     * @Assert\NotBlank(
     *     message="Имя файла должно быть заполнено"
     * )
     * @var string
     */
    protected $name;

    /**
     * @Assert\NotBlank(
     *     message="Расширение файла должно быть заполнено"
     * )
     * @var string
     */
    protected $extension;

    /**
     * @Assert\NotBlank(
     *     message="Имя сущности должно быть заполнено"
     * )
     * @var string
     */
    protected $entityName;

    /**
     * @Assert\NotBlank(
     *     message="Id сущности должен быть заполнен"
     * )
     * @var string
     */
    protected $entityRefKey;

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return $this->entityName;
    }

    /**
     * @return string
     */
    public function getEntityRefKey(): string
    {
        return $this->entityRefKey;
    }

    /**
     * UploadBase64FileDTO constructor.
     * @param string $base64File
     * @param string $name
     * @param string $extension
     * @param string $entityName
     * @param string $entityRefKey
     */
    public function __construct(
        string $base64File,
        string $name,
        string $extension,
        string $entityName,
        string $entityRefKey)
    {
        $this->base64File = $base64File;
        $this->name = $name;
        $this->extension = $extension;
        $this->entityName = $entityName;
        $this->entityRefKey = $entityRefKey;
    }

    /**
     * @return string
     */
    public function getBase64File(): string
    {
        return $this->base64File;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

}