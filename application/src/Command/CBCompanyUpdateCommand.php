<?php

namespace App\Command;

use App\Clients\CBBoss24Client;
use App\Entity\Company;
use App\Service\CBBoss24ServiceUpdater;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use yii\helpers\ArrayHelper;

class CBCompanyUpdateCommand extends Command
{
    private $serviceUpdater;

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'cb:company-update';

    /**
     * CBCompanyUpdateCommand constructor.
     * @param string|null $name
     * @param CBBoss24ServiceUpdater $serviceUpdater
     */
    public function __construct(string $name = null, CBBoss24ServiceUpdater $serviceUpdater)
    {
        $this->serviceUpdater = $serviceUpdater;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates or update a company from ClientBank.')
            ->setHelp('This command allows you to create or update a company...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->serviceUpdater->syncCompany();
        $output->writeln('Company updated, added ' . $result['addCompany'] . ' companies, ' . $result['addCompanyOneC'] . 'add contragent to 1C');
    }
}
