<?php

namespace App\Command;

use App\Clients\CBBoss24Client;
use App\Entity\Company;
use App\Service\CBBoss24ServiceUpdater;
use App\Service\FileService;
use App\Service\FileUpdaterService;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use yii\helpers\ArrayHelper;

class FileUpdateCommand extends Command
{
    private $fileService;

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'file:load';

    /**
     * CBCompanyUpdateCommand constructor.
     * @param string|null $name
     * @param FileUpdaterService $fileService
     */
    public function __construct(string $name = null, FileUpdaterService $fileService)
    {
        $this->fileService = $fileService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates or update a files from OneC.')
            ->setHelp('This command allows you to create or update a files...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->fileService->syncFile();
        $output->writeln('Files updated, added ' . $result . ' files');
    }
}
