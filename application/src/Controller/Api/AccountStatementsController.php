<?php

namespace App\Controller\Api;

use App\Service\AccountStatementToOneCService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/account-statement")
 */
class AccountStatementsController extends AbstractController
{
    /**
     * @Route("/{id}",  methods={"POST"},)
     *
     * @SWG\Parameter(
     *      required=true,
     *      in="path",
     *      name="id",
     *      description="Id",
     *      type="integer",
     *   )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return status",
     * )
     *
     * @Security(name="Bearer")
     * @SWG\Tag(name="AccountStatement")
     *
     * @param int $id
     * @param AccountStatementToOneCService $service
     * @return JsonResponse
     * @throws \Doctrine\ORM\EntityNotFoundException
     */


    public function sendToOneCAction(int $id, AccountStatementToOneCService $service)
    {
        try {
            $response = $service->sendStatement($id);
        }catch (\RuntimeException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }

        return $this->json($response);
    }

}