<?php

namespace App\Controller\Api;

use App\Clients\OneCClient;
use App\Helpers\OneCFilterGetParams;
use App\Wrappers\PhotoWrapper;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/one-c")
 */
class OneCController extends AbstractController
{
    /**
     * @Route("/{path}", name="api_one_c",  methods={"GET", "POST", "PUT"},)
     *
     * @SWG\Parameter(
     *      required=true,
     *      in="path",
     *      name="path",
     *      description="Patch from 1C",
     *      type="string",
     *   )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return content from 1C",
     * )
     *
     * @Security(name="Bearer")
     * @SWG\Tag(name="Proxy")
     *
     * @param string $path
     * @param Request $request
     * @param OneCClient $oneCClient
     * @param OneCFilterGetParams $filterGetParams
     * @param PhotoWrapper $photoWrapper
     * @return JsonResponse
     */
    public function proxyAction(
        string $path,
        Request $request,
        OneCClient $oneCClient,
        OneCFilterGetParams $filterGetParams,
        PhotoWrapper $photoWrapper
    ): JsonResponse
    {
        $getParams = $request->query->all();
        $doWrap = $filterGetParams->filter($getParams);
        $postParams = $request->request->all();
        $method = $request->getMethod();
        $credential = $this->getUser()->getOneCCredential();
        $response = $oneCClient->send(
            $credential,
            $path,
            $method,
            $getParams,
            $postParams
        );
        if ($doWrap) {
            $response = $photoWrapper->wrap($response);
        }
        return $this->json($response);
    }

    /**
     * @Route("/{path}/{ref_key}", name="api_one_c_put",  methods={"PUT", "PATCH"},)
     *
     * @SWG\Parameter(
     *      required=true,
     *      in="path",
     *      name="path",
     *      description="Patch from 1C",
     *      type="string",
     *   )
     * @SWG\Parameter(
     *      required=true,
     *      in="path",
     *      name="ref_key",
     *      description="Ref_key for put request",
     *      type="string",
     *   )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return content from 1C",
     * )
     *
     * @Security(name="Bearer")
     * @SWG\Tag(name="Proxy")
     *
     * @param string $path
     * @param string $ref_key
     * @param Request $request
     * @param OneCClient $oneCClient
     * @return JsonResponse
     */
    public function proxyPutAction(string $path, string $ref_key, Request $request, OneCClient $oneCClient)
    {
        $getParams = $request->query->all();
        $postParams = $request->request->all();
        $method = $request->getMethod();
        $credential = $this->getUser()->getOneCCredential();
        $response = $oneCClient->send($credential, $path, $method, $getParams, $postParams, $ref_key);
        return $this->json($response);
    }

    /**
     * @Route("/", name="api_one_c_all",  methods={"GET"},)
     *
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return all routes from 1C",
     * )
     *
     * @SWG\Tag(name="Proxy")
     *
     * @Security(name="Bearer")
     * @param OneCClient $oneCClient
     * @return JsonResponse
     */
    public function getAllAction(OneCClient $oneCClient)
    {
        $credential = $this->getUser()->getOneCCredential();
        $response = $oneCClient->send($credential, '', Request::METHOD_GET);
        return $this->json($response);
    }
}