<?php

namespace App\Controller\Api;

use App\Clients\OneCClient;
use App\Entity\Company;
use App\Entity\File;
use App\Service\AccountStatementToOneCService;
use App\Service\CBBoss24ServiceUpdater;
use App\Service\FileUpdaterService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/test")
 */
class TestController extends AbstractController
{
    /**
     * @Route("/",  methods={"GET"},)
     * @param CBBoss24ServiceUpdater $service
     * @return JsonResponse
     */
    public function testAction(AccountStatementToOneCService $service)
    {

            $response = $service->sendStatement(1);
        return $this->json(1);
    }

}