<?php

namespace App\Controller\Api;

use App\DTO\UploadBase64FileDTO;
use App\Helpers\Base64FileExtractor;
use App\Helpers\UploadedBase64File;
use App\Service\FileService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @Route("/file", name="api_file")
 */
class FileController extends AbstractFOSRestController
{
    /**
     * @Route("/add", methods={"POST"})
     * @ParamConverter("fileDTO", converter="fos_rest.request_body")
     * @SWG\Parameter(
     *     name="base64File",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="base64File"
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="name"
     * )
     * @SWG\Parameter(
     *     name="extension",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="extension"
     * )
     * @SWG\Parameter(
     *     name="entityRefKey",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="entityRefKey"
     * )
     * @SWG\Parameter(
     *     name="entityName",
     *     in="query",
     *     required=true,
     *     type="string",
     *     description="entityName"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns path to file",
     *     @SWG\Items(
     *          @SWG\Schema(
     *               @SWG\Property(property="path",type="string",description="Path to file"),
     *           ),
     *       ),
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Validation error",
     * )
     *
     * @Security(name="Bearer")
     * @SWG\Tag(name="File")
     *
     * @param UploadBase64FileDTO $fileDTO
     * @param ConstraintViolationListInterface $validationErrors
     * @param FileService $fileService
     * @return mixed
     */
    public function addImage(UploadBase64FileDTO $fileDTO, ConstraintViolationListInterface $validationErrors, FileService $fileService)
    {
        if (\count($validationErrors) > 0) {
            return $this->json($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        try {
            $fileInfo = $fileService->saveFile($fileDTO, $this->getUser());
        }catch (\RuntimeException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }

        return $this->json($fileInfo, 200);
    }
}