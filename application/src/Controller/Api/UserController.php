<?php

namespace App\Controller\Api;

use App\DTO\UserRegistrationDTO;
use App\ProxyFacade\EmployeeFacade;
use App\Service\UserService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @Route("/user", name="api_user")
 */
class UserController extends AbstractFOSRestController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Create user
     * @Route("/add", methods={"POST"})
     * @ParamConverter("userDto", converter="fos_rest.request_body")
     *
     * @SWG\Parameter(
     *     name="username",
     *     in="query",
     *     type="string",
     *     description="Username"
     * )
     * * @SWG\Parameter(
     *     name="password",
     *     in="query",
     *     type="string",
     *     description="Password"
     * )
     * * @SWG\Parameter(
     *     name="oneCUsername",
     *     in="query",
     *     type="string",
     *     description="1C Username"
     * )
     * * @SWG\Parameter(
     *     name="oneCPassword",
     *     in="query",
     *     type="string",
     *     description="1C Password"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Items(
     *          @SWG\Schema(
     *               @SWG\Property(property="token",type="string",description="Access token"),
     *               @SWG\Property(property="refresh_token",type="string",description="Refresh token"),
     *           ),
     *       ),
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Validation error",
     * )
     *
     * @SWG\Tag(name="User")
     *
     * @param UserRegistrationDTO $userDto
     * @param ConstraintViolationListInterface $validationErrors
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(UserRegistrationDTO $userDto, ConstraintViolationListInterface $validationErrors)
    {
        if (\count($validationErrors) > 0) {
            return $this->json($validationErrors, Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->userService->addUser($userDto);
        } catch (UniqueConstraintViolationException $e) {
            return $this->json(['message' => 'Пользователь с таким логином уже существует'], 400);
        } catch (\RuntimeException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }

        return $this->redirectToRoute('api_auth_login', [
            'username' => $userDto->getUsername(),
            'password' => $userDto->getPassword()
        ], 307);
    }

    /**
     * Get roles tree
     * @Route("/get-all-roles", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Return all roles tree",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Validation error",
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     * @param EmployeeFacade $UserFacade
     * @return JsonResponse
     */
    public function treeRolesAction(EmployeeFacade $UserFacade): JsonResponse
    {
        try {
            $credential = $this->getUser()->getOneCCredential();
            $rolesTree = $UserFacade->getRolesTree($credential);
        } catch (\RuntimeException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }

        return $this->json($rolesTree, 200);
    }

    /**
     * Get roles by user
     * @Route("/get-roles-by/{employeeKey}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Return user roles by employeeKey",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Validation error",
     * )
     * @SWG\Tag(name="User")
     * @Security(name="Bearer")
     * @param string $employeeKey
     * @param EmployeeFacade $employeeFacade
     * @return JsonResponse
     */
    public function getRolesByCodeAction(string $employeeKey, EmployeeFacade $employeeFacade): JsonResponse
    {
        try {
            $credential = $this->getUser()->getOneCCredential();
            $roles = $employeeFacade->getRolesByCode($credential, $employeeKey);
        } catch (\RuntimeException $e) {
            return $this->json(['message' => $e->getMessage()], 400);
        }

        return $this->json(['roles' => $roles], 200);
    }

}