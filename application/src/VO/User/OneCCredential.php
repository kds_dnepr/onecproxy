<?php

namespace App\VO\User;

use http\Exception\BadQueryStringException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;

class OneCCredential
{

    /**
     * @Assert\Length(min=5
     *     message = "Bad username 1C"
     * ),
     * @var string
     */
    protected $username;

    /**
     * @Assert\Length(min=5
     *     message = "Bad password 1C"
     * ),
     * @var string
     */
    protected $password;

    /**
     * OneCCredential constructor.
     * @param $username
     * @param $password
     */
    public function __construct($username, $password)
    {
            $this->username =$username;
            $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

}