<?php

namespace App\Entity;

use App\VO\User\OneCCredential;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(
 *     fields={"username"}
 * )
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(type="string", name="email", length=255, unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(type="string", name="email_canonical", length=255, unique=false, nullable=true))
 * })
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="one_c_credential", nullable=false)
     * @var OneCCredential
     */
    protected $oneCCredential;

    public function __construct()
    {
        parent::__construct();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return OneCCredential
     */
    public function getOneCCredential(): OneCCredential
    {
        return $this->oneCCredential;
    }

    /**
     * @param OneCCredential $oneCCredential
     * @return User
     */
    public function setOneCCredential(OneCCredential $oneCCredential): User
    {
        $this->oneCCredential = $oneCCredential;
        return $this;
    }

}