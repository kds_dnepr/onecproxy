<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyAccountRepository")
 */
class CompanyAccount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $mfo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $account_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $currency;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_visible;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_sync_with_telegram;

    /**
     * @ORM\Column(type="integer")
     */
    private $overdraft;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bank_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bank_alias;

    /**
     * @ORM\Column(type="integer")
     */
    private $bank_id;

    /**
     *
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Company")
     * @ORM\JoinColumn(nullable=false)
     */

    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refKey;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMfo(): ?string
    {
        return $this->mfo;
    }

    public function setMfo(string $mfo): self
    {
        $this->mfo = $mfo;

        return $this;
    }

    public function getAccountNumber(): ?string
    {
        return $this->account_number;
    }

    public function setAccountNumber(string $account_number): self
    {
        $this->account_number = $account_number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->is_visible;
    }

    public function setIsVisible(bool $is_visible): self
    {
        $this->is_visible = $is_visible;

        return $this;
    }

    public function getIsSyncWithTelegram(): ?bool
    {
        return $this->is_sync_with_telegram;
    }

    public function setIsSyncWithTelegram(bool $is_sync_with_telegram): self
    {
        $this->is_sync_with_telegram = $is_sync_with_telegram;

        return $this;
    }

    public function getOverdraft(): ?int
    {
        return $this->overdraft;
    }

    public function setOverdraft(int $overdraft): self
    {
        $this->overdraft = $overdraft;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bank_name;
    }

    public function setBankName(string $bank_name): self
    {
        $this->bank_name = $bank_name;

        return $this;
    }

    public function getBankAlias(): ?string
    {
        return $this->bank_alias;
    }

    public function setBankAlias(string $bank_alias): self
    {
        $this->bank_alias = $bank_alias;

        return $this;
    }

    public function getBankId(): ?int
    {
        return $this->bank_id;
    }

    public function setBankId(int $bank_id): self
    {
        $this->bank_id = $bank_id;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefKey()
    {
        return $this->refKey;
    }

    /**
     * @param mixed $refKey
     * @return CompanyAccount
     */
    public function setRefKey($refKey): self
    {
        $this->refKey = $refKey;

        return $this;
    }
}
