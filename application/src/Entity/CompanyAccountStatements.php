<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyAccountStatementsRepository")
 */
class CompanyAccountStatements
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @var CompanyAccount
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CompanyAccount")
     * @ORM\JoinColumn(nullable=false)
     */
    
    private $aaccount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $baccount;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_order;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $contragent;

    /**
     * @ORM\Column(type="string", length=2048)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $num_order;

    /**
     * @ORM\Column(type="integer")
     */
    private $sum;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $transaction_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAAccount(): ?CompanyAccount
    {
        return $this->aaccount;
    }

    public function setAAccount(CompanyAccount $account): self
    {
        $this->aaccount = $account;

        return $this;
    }

    public function getBAccount(): ?string
    {
        return $this->baccount;
    }

    public function setBAccount(string $account): self
    {
        $this->baccount = $account;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getDateOrder(): ?\DateTimeInterface
    {
        return $this->date_order;
    }

    public function setDateOrder(\DateTimeInterface $date_order): self
    {
        $this->date_order = $date_order;

        return $this;
    }

    public function getContragent(): ?string
    {
        return $this->contragent;
    }

    public function setContragent(string $contragent): self
    {
        $this->contragent = $contragent;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumOrder(): ?string
    {
        return $this->num_order;
    }

    public function setNumOrder(string $num_order): self
    {
        $this->num_order = $num_order;

        return $this;
    }

    public function getSum(): ?int
    {
        return $this->sum;
    }

    public function setSum(int $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getTransactionType(): ?string
    {
        return $this->transaction_type;
    }

    public function setTransactionType(string $transaction_type): self
    {
        $this->transaction_type = $transaction_type;

        return $this;
    }
}
