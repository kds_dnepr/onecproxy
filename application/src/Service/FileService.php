<?php

namespace App\Service;

use App\DTO\UploadBase64FileDTO;
use App\DTO\UploadedOneCFileDTO;
use App\Entity\File;
use App\Entity\User;
use App\Helpers\Base64FileExtractor;
use App\Helpers\UploadedBase64File;
use App\ProxyFacade\UploadedDataFacade;
use Doctrine\ORM\EntityManagerInterface;

class FileService
{
    /**
     * @var Base64FileExtractor
     */
    private $base64FileExtractor;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $uploadedDataFacade;

    /**
     * FileService constructor.
     * @param Base64FileExtractor $base64FileExtractor
     * @param EntityManagerInterface $em
     * @param UploadedDataFacade $uploadedDataFacade
     */
    public function __construct(Base64FileExtractor $base64FileExtractor, EntityManagerInterface $em, UploadedDataFacade $uploadedDataFacade)
    {
        $this->base64FileExtractor = $base64FileExtractor;
        $this->em = $em;
        $this->uploadedDataFacade = $uploadedDataFacade;
    }

    /**
     * @param UploadBase64FileDTO $fileDTO
     * @param User $user
     * @return array
     */
    public function saveFile(UploadBase64FileDTO $fileDTO, User $user): array
    {
        $base64Image = $this->base64FileExtractor->extractBase64String($fileDTO->getBase64File());
        $imageFile = new UploadedBase64File(
            $base64Image,
            $fileDTO->getName(),
            $fileDTO->getExtension(),
            $fileDTO->getEntityName()
        );

        $newFile = new File();
        $newFile
            ->setCreatedBy($user)
            ->setExtension($fileDTO->getExtension())
            ->setName($fileDTO->getName())
            ->setPath($imageFile->getFilePath())
            ->setEntityName($fileDTO->getEntityName())
            ->setEntityRefKey($fileDTO->getEntityRefKey());


        $newOneCFile = $this->sendFileOneC($fileDTO, $imageFile->getFilePath(), $user);

        $newFile
            ->setCode($newOneCFile['Code'])
            ->setFileRefKey($newOneCFile['Ref_Key']);

        $this->em->persist($newFile);
        $this->em->flush();

        return [
            'ref_key' => $newOneCFile['Ref_Key'],
            'web_path' => $newOneCFile['Description']
        ];
    }

    /**
     * @param UploadedOneCFileDTO $fileDTO
     * @param User $user
     * @return bool
     */
    public function saveFileFromOneC(UploadedOneCFileDTO $fileDTO, User $user): bool
    {
        $base64Image = $fileDTO->getBase64File();
        $imageFile = new UploadedBase64File(
            $base64Image,
            $fileDTO->getName(),
            $fileDTO->getExtension(),
            $fileDTO->getEntityName());

        $newFile = new File();
        $newFile
            ->setCreatedBy($user)
            ->setExtension($fileDTO->getExtension())
            ->setName($fileDTO->getName())
            ->setPath($imageFile->getFilePath())
            ->setEntityName($fileDTO->getEntityName())
            ->setEntityRefKey($fileDTO->getEntityRefKey())
            ->setCode($fileDTO->getCode())
            ->setFileRefKey($fileDTO->getFileRefKey());

        $this->em->persist($newFile);
        $this->em->flush();

        return true;
    }

    private function sendFileOneC(UploadBase64FileDTO $fileDTO, $path, User $user): array
    {
        $webPath = '/' . str_replace($_ENV['WEB_ROOT_PATH'], '', $path);
        $fullName = $fileDTO->getName() . '.' . $fileDTO->getExtension();
        $dataPost = [
            'Description' => $webPath,
            'ИмяФайла' => $fullName,
            'ХранимыеДанные_Type' => 'application/octet-stream',
            'ХранимыеДанные_Base64Data' => $this->base64FileExtractor->extractBase64String($fileDTO->getBase64File()),
            'ВладелецВложения' => $fileDTO->getEntityRefKey(),
            'ВладелецВложения_Type' => 'StandardODATA.' . $fileDTO->getEntityName()
        ];

        return $this->uploadedDataFacade->addNewFile(
            $user->getOneCCredential(),
            $dataPost
        );
    }
}