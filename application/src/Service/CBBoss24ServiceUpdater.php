<?php


namespace App\Service;


use App\Clients\CBBoss24Client;
use App\Clients\OneCClient;
use App\Entity\Company;
use App\Entity\CompanyAccount;
use App\Entity\CompanyAccountStatements;
use App\ProxyFacade\AccountFacade;
use App\ProxyFacade\ContragentFacade;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use yii\helpers\ArrayHelper;

class CBBoss24ServiceUpdater
{
    private $CBBoss24Client;
    private $oneCClient;
    private $em;
    private $accountOneC;
    private $contragentOneC;


    /**
     * @param CBBoss24Client $boss24Client
     * @param EntityManagerInterface $em
     * @param OneCClient $client
     * @param AccountFacade $accountOneC
     * @param ContragentFacade $contragentOneC
     */
    public function __construct(
        CBBoss24Client $boss24Client,
        EntityManagerInterface $em,
        OneCClient $client,
        AccountFacade $accountOneC,
        ContragentFacade $contragentOneC
    )
    {
        $this->CBBoss24Client = $boss24Client;
        $this->oneCClient = $client;
        $this->em = $em;
        $this->accountOneC = $accountOneC;
        $this->contragentOneC = $contragentOneC;
    }


    public function syncCompany()
    {
        $addCompany = 0;
        $addCompanyOneC = 0;
        $oneCContragent = $this->contragentOneC->getAllContragent();
        $oneCContragentIndexByOKPO = ArrayHelper::index($oneCContragent, 'ОКПО');

        $companyesRemote = $this->CBBoss24Client->getCompanyes();
        $companyesDB = $this->em
            ->getRepository(Company::class)
            ->createQueryBuilder('c')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $companyesDBIndexByOkpo = ArrayHelper::index($companyesDB, 'okpo');
        foreach ($companyesRemote as $item) {
            if (!isset($companyesDBIndexByOkpo[$item['okpo']])) {
                $newCompany = new Company();
                $newCompany
                    ->setCompanyId($item['id'])
                    ->setEmail($item['email'])
                    ->setIsActive($item['is_active'])
                    ->setLogo($item['logo'])
                    ->setName($item['name'])
                    ->setOkpo($item['okpo'])
                    ->setPhone($item['phone']);

                $addCompany++;

                if (!isset($oneCContragentIndexByOKPO[$newCompany->getOkpo()])) {
                    $newContragentOneC = $this->contragentOneC->createContragent($newCompany);
                    $addCompanyOneC++;
                    $newCompany->setRefKey($newContragentOneC['Ref_Key']);
                } else {
                    $newCompany->setRefKey($oneCContragentIndexByOKPO[$newCompany->getOkpo()]['Ref_Key']);
                }

                $this->em->persist($newCompany);
            }
        }
        $this->em->flush();

        return ['addCompany' => $addCompany, 'addCompanyOneC' => $addCompanyOneC];
    }

    public function syncCompanyAccount()
    {
        $addCompanyAcc = 0;
        $addCompanyAccOneC = 0;

        $oneCAccount = $this->accountOneC->getAllAccount();
        $oneCAccountIndexByAccount = ArrayHelper::index($oneCAccount, 'БанкСчет');

        $companyAccDB = $this->em
            ->getRepository(CompanyAccount::class)
            ->createQueryBuilder('c')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $companyAccDB = ArrayHelper::index($companyAccDB, 'account_number');

        $q = $this->em->getRepository(Company::class)
            ->createQueryBuilder('c')
            ->getQuery();

        $iterableResult = $q->iterate();
        foreach ($iterableResult as $row) {
            $companyAcc = $this->CBBoss24Client->getCompanyAccount($row[0]->getOkpo());
            foreach ($companyAcc as $account) {
                if (!isset($companyAccDB[$account['account_number']])) {

                    $newCompanyAcc = new CompanyAccount();
                    $newCompanyAcc
                        ->setMfo($account['mfo'])
                        ->setAccountNumber($account['account_number'])
                        ->setBankAlias($account['bank_alias'])
                        ->setBankId($account['bank_id'])
                        ->setBankName($account['bank_name'])
                        ->setCurrency($account['currency'])
                        ->setIsVisible($account['is_visible'])
                        ->setIsSyncWithTelegram($account['is_sync_with_telegram'])
                        ->setOverdraft($account['overdraft'])
                        ->setCompany($row[0])
                        ->setName($account['name'])
                        ->setAlias($account['alias']);
                    $addCompanyAcc++;

                    if (!isset($oneCAccountIndexByAccount[$newCompanyAcc->getAccountNumber()])) {
                        $newAccountOneC = $this->accountOneC->createAccount($newCompanyAcc);
                        $newCompanyAcc->setRefKey($newAccountOneC['Ref_Key']);
                        $addCompanyAccOneC++;
                    } else {
                        $newCompanyAcc->setRefKey($oneCAccountIndexByAccount[$newCompanyAcc->getAccountNumber()]['Ref_Key']);
                    }

                    $this->em->persist($newCompanyAcc);
                }
            }
        }
        $this->em->flush();

        return ['addAccount' => $addCompanyAcc, 'addAccountOneC' => $addCompanyAccOneC];
    }

    public function syncAccountStatements($dateFrom = null)
    {
        $addAccStatements = 0;

        $companyAccStatementsDB = $this->em
            ->getRepository(CompanyAccountStatements::class)
            ->createQueryBuilder('c')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        $companyAccStatementsDB = ArrayHelper::index($companyAccStatementsDB, 'num_order');

        $q = $this->em->getRepository(CompanyAccount::class)
            ->createQueryBuilder('c')
            ->getQuery();

        $iterableResult = $q->iterate();
        foreach ($iterableResult as $row) {
            $companyAccStatements = $this->CBBoss24Client->getCompanyAccountStatements(
                $row[0]->getCompany()->getOkpo(),
                $row[0]->getAccountNumber(),
                $dateFrom,
                $dateFrom ? date('d-m-y') : null);
            if (isset($companyAccStatements['data'])) {
                foreach ($companyAccStatements['data'] as $statement) {
                    if (!isset($companyAccStatementsDB[$statement['num_order']])) {

                        $newAccStatement = new CompanyAccountStatements();
                        $newAccStatement->setCurrency($statement['currency'])
                            ->setSum($statement['sum'])
                            ->setAAccount($row[0])
                            ->setBAccount($statement['account_number'])
                            ->setContragent(json_encode($statement['counterparty']))
                            ->setDateOrder(new \DateTime($statement['date_order']))
                            ->setDescription($statement['description'])
                            ->setNumOrder($statement['num_order'])
                            ->setTransactionType($statement['transaction_type']);

                        $addAccStatements++;
                        $this->em->persist($newAccStatement);
                    }
                }
            }
        }
        $this->em->flush();

        return ['add' => $addAccStatements];
    }
}