<?php

namespace App\Service;

use App\Clients\OneCClient;
use App\DTO\UserRegistrationDTO;
use App\Entity\User;
use App\ProxyFacade\UsersFacade;
use App\Repository\UserRepository;
use App\VO\User\OneCCredential;


class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    private $UserFacadeClient;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param UsersFacade $UserFacadeClient
     */
    public function __construct(UserRepository $userRepository, UsersFacade $UserFacadeClient)
    {
        $this->userRepository = $userRepository;
        $this->UserFacadeClient = $UserFacadeClient;
    }

    public function addUser(UserRegistrationDTO $userDto)
    {
        $oneCCredential = new OneCCredential($userDto->getOneCUsername(),$userDto->getOneCPassword());

        $userRole = $this->UserFacadeClient->getRolesByUser($oneCCredential);

        $user = new User();
        $user
            ->setUsername($userDto->getUsername())
            ->setEmail($userDto->getUsername())
            ->setPlainPassword($userDto->getPassword())
            ->setEnabled(true)
            ->setOneCCredential($oneCCredential)
            ->setRoles([$userRole])
            ->setSuperAdmin(false);

        $this->userRepository->create($user);
    }
}