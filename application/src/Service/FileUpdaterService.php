<?php

namespace App\Service;

use App\DTO\UploadBase64FileDTO;
use App\DTO\UploadedOneCFileDTO;
use App\Entity\User;
use App\ProxyFacade\UploadedDataFacade;
use Doctrine\ORM\EntityManagerInterface;

class FileUpdaterService
{
    /**
     * @var UploadedDataFacade
     */
    protected $fileFacade;
    /**
     * @var FileService
     */
    protected $fileService;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * FileUpdaterService constructor.
     * @param UploadedDataFacade $fileFacade
     * @param FileService $fileService
     * @param EntityManagerInterface $em
     */
    public function __construct(UploadedDataFacade $fileFacade, FileService $fileService, EntityManagerInterface $em)
    {
        $this->fileFacade = $fileFacade;
        $this->fileService = $fileService;
        $this->em = $em;
    }

    /**
     * Создает файлы на остовании 1С все с одним и тем же названием перезапишутся,
     * базу локальную нужно очистить перед запуском
     */
    public function syncFile()
    {
        /**
         * @var User
         */
        $adminUser = $this->em
            ->getRepository(User::class)
            ->getAdmin();
        $page = 0;
        do {
            $response = $this->fileFacade->getFiles($adminUser->getOneCCredential(), $page, 50);
            foreach ($response['value'] as $item) {
                $file = explode('.', $item['ИмяФайла']);
                $fileDTO = new UploadedOneCFileDTO(
                    $item['ХранимыеДанные_Base64Data'],
                    $item['Ref_Key'] . $file[0],
                    end($file),
                    $item['ВладелецВложения_Type'],
                    $item['ВладелецВложения'],
                    $item['Ref_Key'],
                    $item['Code']
                );
                $this->fileService->saveFileFromOneC($fileDTO, $adminUser);
            }
            $page += 50;
        } while (count($response['value']) > 0);

        return $page;

    }

}