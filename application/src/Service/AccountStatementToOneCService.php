<?php


namespace App\Service;


use App\Entity\Company;
use App\Entity\CompanyAccount;
use App\Entity\CompanyAccountStatements;
use App\ProxyFacade\AccountFacade;
use App\ProxyFacade\AccountStatementInFacade;
use App\ProxyFacade\AccountStatementOutFacade;
use App\ProxyFacade\BankFacade;
use App\ProxyFacade\ContragentFacade;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Query;
use yii\helpers\ArrayHelper;

class AccountStatementToOneCService
{

    private $em;
    private $outAccStatementOneC;
    private $inAccStatementOneC;
    private $accountOneC;
    private $allAccOneC;

    private $contragentOneC;
    private $allContragentOneC;


    /**
     * AccountStatementToOneCService constructor.
     * @param EntityManagerInterface $em
     * @param AccountStatementOutFacade $outAccStatementOneC
     * @param AccountStatementInFacade $inAccStatementOneC
     * @param AccountFacade $accountOneC
     * @param ContragentFacade $contragentFacade
     */
    public function __construct(
        EntityManagerInterface $em,
        AccountStatementOutFacade $outAccStatementOneC,
        AccountStatementInFacade $inAccStatementOneC,
        AccountFacade $accountOneC,
        ContragentFacade $contragentFacade
    )
    {
        $this->em = $em;
        $this->outAccStatementOneC = $outAccStatementOneC;
        $this->inAccStatementOneC = $inAccStatementOneC;
        $this->accountOneC = $accountOneC;
        $oneCAccount = $this->accountOneC->getAllAccount();
        $this->allAccOneC = ArrayHelper::index($oneCAccount, 'БанкСчет');

        $this->contragentOneC = $contragentFacade;
        $oneCContragent = $this->contragentOneC->getAllContragent();
        $this->allContragentOneC = ArrayHelper::index($oneCContragent, 'ОКПО');
    }

    public function sendStatement(int $id)
    {
        /**
         * @var CompanyAccountStatements $accStatement
         */

        $accStatement = $this->em
            ->getRepository(CompanyAccountStatements::class)
            ->find($id);
        if (!$accStatement) {
            throw new EntityNotFoundException('This Account Statement not found');
        }

        if ($accStatement->getTransactionType() === 'D') {
            return $this->outStatementSend($accStatement);
        } else {
            return $this->inStatementSend($accStatement);
        }

    }

    public function outStatementSend(CompanyAccountStatements $accountStatements)
    {
        $bAccRefKey = $this->checkAcc($accountStatements->getBAccount(), $accountStatements);
        $bContragent = json_decode($accountStatements->getContragent(), 1);
        $contragentRefKey = $this->checkContragent($bContragent['okpo']);
        $this->outAccStatementOneC->createAccountStatementOut($accountStatements, $bAccRefKey, $contragentRefKey);
        return true;
    }

    private function checkAcc($accNum, CompanyAccountStatements $accountStatements)
    {
        if (!isset($this->allAccOneC[$accNum])) {
            $contragent = json_decode($accountStatements->getContragent(), 1);
            $response = $this->accountOneC->createAccountRaw(
                $contragent['title'],
                $accountStatements->getBAccount(),
                $contragent['okpo'],
                $contragent['title'],
                BankFacade::BANK_KEY[$contragent['title']]
            );
            return $response['Ref_Key'];
        }
        return $this->allAccOneC[$accNum]['Ref_Key'];
    }

    public function checkContragent($okpo, CompanyAccountStatements $accountStatements)
    {
        if (!isset($this->allContragentOneC[$okpo])) {
            $contragent = json_decode($accountStatements->getContragent(), 1);
            $newCompany = new Company();
            $newCompany
                ->setCompanyId($contragent['id'])
                ->setEmail($contragent['email'])
                ->setIsActive(true)
                ->setName($contragent['title'])
                ->setOkpo($contragent['okpo']);

            $newContragentOneC = $this->contragentOneC->createContragent($newCompany);
            $newCompany->setRefKey($newContragentOneC['Ref_Key']);
            $this->em->persist($newCompany);
            $this->em->flush();
            return $newContragentOneC['Ref_Key'];
        }
        return $this->allContragentOneC[$okpo]['Ref_Key'];
    }

    public function inStatementSend(CompanyAccountStatements $accountStatements)
    {
        $bAccRefKey = $this->checkAcc($accountStatements->getBAccount(), $accountStatements);
        $bContragent = json_decode($accountStatements->getContragent(), 1);
        $contragentRefKey = $this->checkContragent($bContragent['okpo'], $accountStatements);
        $this->inAccStatementOneC->createAccountStatementIn($accountStatements, $bAccRefKey, $contragentRefKey);
        return true;
    }
}