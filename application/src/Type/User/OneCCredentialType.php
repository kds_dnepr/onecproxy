<?php

namespace App\Type\User;

use App\VO\User\OneCCredential;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class OneCCredentialType extends Type
{
    const NAME = "one_c_credential";

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $decodeData = json_decode(base64_decode($value), true);
        return new OneCCredential($decodeData['username'], $decodeData['password']);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $encodeData = [
            'username' => $value->getUsername(),
            'password' => $value->getPassword(),
        ];
        return base64_encode(json_encode($encodeData));
    }

    /**
     * @inheritDoc
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @inheritdoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::NAME;
    }
}