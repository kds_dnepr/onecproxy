<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324072157 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX uniq_957a6479a0d96fbf');
        $this->addSql('ALTER TABLE fos_user ALTER email DROP NOT NULL');
        $this->addSql('ALTER TABLE fos_user ALTER email TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ALTER email_canonical DROP NOT NULL');
        $this->addSql('ALTER TABLE fos_user ALTER email_canonical TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ALTER one_ccredential TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ALTER one_ccredential DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE fos_user ALTER one_ccredential TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE fos_user ALTER one_ccredential DROP DEFAULT');
        $this->addSql('ALTER TABLE fos_user ALTER email SET NOT NULL');
        $this->addSql('ALTER TABLE fos_user ALTER email TYPE VARCHAR(180)');
        $this->addSql('ALTER TABLE fos_user ALTER email_canonical SET NOT NULL');
        $this->addSql('ALTER TABLE fos_user ALTER email_canonical TYPE VARCHAR(180)');
        $this->addSql('CREATE UNIQUE INDEX uniq_957a6479a0d96fbf ON fos_user (email_canonical)');
    }
}
