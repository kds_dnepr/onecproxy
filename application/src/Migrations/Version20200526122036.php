<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200526122036 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE company_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE company_account_statements_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE company_account (id INT NOT NULL, company_id INT NOT NULL, mfo BIGINT NOT NULL, account_number VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, currency VARCHAR(5) NOT NULL, is_visible BOOLEAN NOT NULL, is_sync_with_telegram BOOLEAN NOT NULL, overdraft INT NOT NULL, bank_name VARCHAR(255) NOT NULL, bank_alias VARCHAR(255) NOT NULL, bank_id INT NOT NULL, alias VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5D644C73979B1AD6 ON company_account (company_id)');
        $this->addSql('CREATE TABLE company_account_statements (id INT NOT NULL, account_id INT NOT NULL, currency VARCHAR(5) NOT NULL, date_order TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, contragent VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, num_order VARCHAR(255) NOT NULL, sum INT NOT NULL, transaction_type VARCHAR(5) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_ED09D5489B6B5FBA ON company_account_statements (account_id)');
        $this->addSql('ALTER TABLE company_account ADD CONSTRAINT FK_5D644C73979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE company_account_statements ADD CONSTRAINT FK_ED09D5489B6B5FBA FOREIGN KEY (account_id) REFERENCES company_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE company_account_statements DROP CONSTRAINT FK_ED09D5489B6B5FBA');
        $this->addSql('DROP SEQUENCE company_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE company_account_statements_id_seq CASCADE');
        $this->addSql('DROP TABLE company_account');
        $this->addSql('DROP TABLE company_account_statements');
    }
}
