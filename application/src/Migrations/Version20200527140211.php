<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527140211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE company_account_statements DROP CONSTRAINT fk_ed09d5489b6b5fba');
        $this->addSql('DROP INDEX idx_ed09d5489b6b5fba');
        $this->addSql('ALTER TABLE company_account_statements ADD baccount VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE company_account_statements RENAME COLUMN account_id TO aaccount_id');
        $this->addSql('ALTER TABLE company_account_statements ADD CONSTRAINT FK_ED09D548823CCFD8 FOREIGN KEY (aaccount_id) REFERENCES company_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_ED09D548823CCFD8 ON company_account_statements (aaccount_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE company_account_statements DROP CONSTRAINT FK_ED09D548823CCFD8');
        $this->addSql('DROP INDEX IDX_ED09D548823CCFD8');
        $this->addSql('ALTER TABLE company_account_statements DROP baccount');
        $this->addSql('ALTER TABLE company_account_statements RENAME COLUMN aaccount_id TO account_id');
        $this->addSql('ALTER TABLE company_account_statements ADD CONSTRAINT fk_ed09d5489b6b5fba FOREIGN KEY (account_id) REFERENCES company_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_ed09d5489b6b5fba ON company_account_statements (account_id)');
    }
}
